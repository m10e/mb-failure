// ---------- ---------- ---------- ---------- ---------- ---------- ----------

extern crate failure;

// ---------- ---------- ---------- ---------- ---------- ---------- ----------

use failure::Error;

// ---------- ---------- ---------- ---------- ---------- ---------- ----------

// FIXME - Different exit code?

pub fn print_error(e: Error) {
    println!("Error:");
    for cause in e.causes() {
        println!("  cause: {}", cause);
    }
    println!("{}", e.backtrace());
}

pub fn print_if_error<A>(r: Result<A, Error>) {
    match r {
        Ok(_) => { /* All went well. */},
        Err(e) => print_error(e)
    }
}

// ---------- ---------- ---------- ---------- ---------- ---------- ----------
